# Mediscreen-Eureka-Server

This is the Eureka-Server for Mediscreen-Module

You can run the current

## Getting Started

run the project with a command : ``mvn spring-boot:run``

Get tests results through : ``mvn clean verify``

Install the jar-with-dependencies with :  ``mvn clean install``

Running the app from docker : On the source folder of this application, you can build the image docker to your local Docker damon using:
``mvn compile jib:dockerBuild``
Get the docker image : run ``docker run -p 9102:9102 mediscreen-eureka-server:1.0.0``

## Technical:

1. Java 17
2. Framework: Spring Boot v2.7.5
3. Current version : 2.0.0

### Running App

You can run the application in two different ways:

1. Run it from docker-compose available on [mediscreen-module](https://gitlab.com/mediscreen-apps/mediscreen-modules)
   Don't forget to specify witch image (corresponding to the version) you want to run
   ___ On the two following method, you will need to run the [config-server](https://gitlab.com/mediscreen-apps/config-server) first.
2. import the code into an IDE of your choice and run the Application.java to launch the application.
3. Or import the code, unzip it, open the console, go to the root folder that contains the gradlew file, then execute
   the below command to launch the application.
```bash
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.cloud.config.uri=http://localhost:9101"
```